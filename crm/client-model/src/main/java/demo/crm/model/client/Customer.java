package demo.crm.model.client;

import java.io.Serializable;
import java.util.List;

public class Customer implements Serializable {

  private String memberId;
  private String firstName;
  private String lastName;
  private List<PhoneNumber> phoneNumbers;

  public Customer() {
    // for GWT-RPC
  }

  public Customer(String memberId, String firstName, String lastName, List<PhoneNumber> phoneNumbers) {
    this.memberId = memberId;
    this.firstName = firstName;
    this.lastName = lastName;
    this.phoneNumbers = phoneNumbers;
  }

  public String getMemberId() {
    return memberId;
  }

  public Customer setMemberId(String memberId) {
    this.memberId = memberId;
    return this;
  }

  public String getFirstName() {
    return firstName;
  }

  public Customer setFirstName(String firstName) {
    this.firstName = firstName;
    return this;
  }

  public String getLastName() {
    return lastName;
  }

  public Customer setLastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

  public List<PhoneNumber> getPhoneNumbers() {
    return phoneNumbers;
  }

  public Customer setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
    this.phoneNumbers = phoneNumbers;
    return this;
  }
}
