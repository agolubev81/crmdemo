package demo.crm.model.client;

import java.io.Serializable;

public class PhoneNumber implements Serializable {
  String number;
  String description;

  public PhoneNumber() {
    // for GWT-RPC
  }

  public PhoneNumber(String number, String description) {
    this.number = number;
    this.description = description;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public String toString() {
    return number;
  }
}
