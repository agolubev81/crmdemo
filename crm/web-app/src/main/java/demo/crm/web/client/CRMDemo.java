package demo.crm.web.client;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiFactory;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.ContentPanel.ContentPanelAppearance;
import com.sencha.gxt.widget.core.client.Dialog;
import com.sencha.gxt.widget.core.client.box.AlertMessageBox;
import com.sencha.gxt.widget.core.client.box.ConfirmMessageBox;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer;
import com.sencha.gxt.widget.core.client.event.HideEvent;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.grid.GridView;
import demo.crm.model.client.Customer;
import demo.crm.model.client.PhoneNumber;
import demo.crm.web.server.api.CrmAdministratorService;
import demo.crm.web.server.api.CrmAdministratorServiceAsync;

public class CRMDemo implements EntryPoint, IsWidget {

  private static CRMDemoUiBinder uiBinder = GWT.create(CRMDemoUiBinder.class);

  interface CRMDemoUiBinder extends UiBinder<ContentPanel, CRMDemo> {}
  private static final CustomerProperties gridProperties = GWT.create(CustomerProperties.class);

  interface EditorDriver extends SimpleBeanEditorDriver<Customer, CustomerEditor> {}
  private EditorDriver driver = GWT.create(EditorDriver.class);

  private static CrmAdministratorServiceAsync _crmService = GWT.create(CrmAdministratorService.class);

  // main panel
  private ContentPanel widget;

  // info panel with elements
  @UiField
  ContentPanel infoPanel;
  @UiField
  Grid<Customer> grid;
  @UiField(provided = true)
  com.sencha.gxt.widget.core.client.grid.ColumnModel<Customer> columnModel;
  @UiField(provided = true)
  ListStore<Customer> listStore;
  @UiField
  GridView<Customer> gridView;
  @UiField
  Button addButton;
  @UiField
  Button removeButton;
  @UiField
  Button editButton;

  // editor panel with elements
  @UiField
  ContentPanel editorPanel;
  @UiField
  CustomerEditor customerEditor;
  @UiField
  Button submitButton;
  @UiField
  Button cancelButton;

  @Override
  public Widget asWidget() {
    if (widget == null) {
      columnModel = initColumModel();
      listStore = initListStore();

      widget = uiBinder.createAndBindUi(this);

      // Auto expand the name column
      gridView.setAutoExpandColumn(columnModel.getColumn(0));

      infoPanel.add(grid);
      infoPanel.addButton(addButton);
      addButton.addClickHandler(new AddButtonHandler());
      infoPanel.addButton(editButton);
      editButton.addClickHandler(new EditButtonHandler());
      infoPanel.addButton(removeButton);
      removeButton.addClickHandler(new RemoveButtonHandler());
      infoPanel.setButtonAlign(BoxLayoutContainer.BoxLayoutPack.CENTER);

      editorPanel.add(customerEditor);
      driver.initialize(customerEditor);
      editorPanel.addButton(submitButton);
      submitButton.addClickHandler(new SubmitButtonHandler());
      editorPanel.addButton(cancelButton);
      cancelButton.addClickHandler(new CancelButtonHandler());
      editorPanel.setButtonAlign(BoxLayoutContainer.BoxLayoutPack.CENTER);

      widget.add(infoPanel);
    }

    return widget;
  }

  private ColumnModel<Customer> initColumModel() {
    ColumnConfig<Customer, String> nameCol = new ColumnConfig<Customer, String>(gridProperties.firstName(), 50, "First Name");
    nameCol.setWidth(30);
    ColumnConfig<Customer, String> symbolCol = new ColumnConfig<Customer, String>(gridProperties.lastName(), 100, "Last Name");
    symbolCol.setWidth(30);
    ColumnConfig<Customer, String> phonesCol = new ColumnConfig<Customer, String>(new PhonesStringValueProvider(), 100, "Phone Numbers");
    phonesCol.setWidth(50);

    List<ColumnConfig<Customer, ?>> columns = new ArrayList<ColumnConfig<Customer, ?>>();
    columns.add(nameCol);
    columns.add(symbolCol);
    columns.add(phonesCol);

    ColumnModel<Customer> columnModel = new ColumnModel<Customer>(columns);

    return columnModel;
  }

  private ListStore<Customer> initListStore() {
    final ListStore<Customer> listStore = new ListStore<Customer>(gridProperties.key());
    _crmService.getCustomers(new AsyncCallback<Customer[]>() {
      @Override
      public void onSuccess(Customer[] result) {
        for (Customer customer : result) {
          listStore.add(customer);
        }
      }
      @Override
      public void onFailure(Throwable caught) {
        new AlertMessageBox("Alert", "Can't get customers").show();
      }
    });
    return listStore;
  }

  @Override
  public void onModuleLoad() {
    RootPanel.get().add(asWidget());
  }

  @UiFactory
  public ContentPanel createContentPanel(ContentPanelAppearance appearance) {
    return new ContentPanel(appearance);
  }

  private static class PhonesStringValueProvider implements ValueProvider<Customer, String> {
    @Override
    public String getValue(Customer object) {
      List<PhoneNumber> phoneNumbers = object.getPhoneNumbers();
      String result;
      if (phoneNumbers == null || phoneNumbers.size() == 0) {
        result = "No phones yet";
      } else {
        result = "";
        for (PhoneNumber phoneNumber : phoneNumbers) {
          result += phoneNumber + ";";
        }
      }
      return result;
    }

    @Override
    public void setValue(Customer object, String value) {
    }

    @Override
    public String getPath() {
      return null;
    }
  }

  private class AddButtonHandler implements ClickHandler {
    @Override
    public void onClick(ClickEvent event) {
      driver.edit(new Customer(null, "Write first name", "Write last name", new ArrayList()));
      widget.add(editorPanel);
    }
  }

  private class EditButtonHandler implements ClickHandler {
    @Override
    public void onClick(ClickEvent event) {
      final Customer selectedItem = grid.getSelectionModel().getSelectedItem();
      if (selectedItem != null) {
        driver.edit(selectedItem);
        widget.add(editorPanel);
      }
    }
  }

  private class RemoveButtonHandler implements ClickHandler {
    private final ConfirmMessageBox mb;
    private final AlertMessageBox amb;

    private RemoveButtonHandler() {
      amb = new AlertMessageBox("Alert", "Can't remove customer");
      mb = new ConfirmMessageBox("Confirm", "Are you sure to remove?");
      mb.addHideHandler(new HideEvent.HideHandler() {
        public void onHide(HideEvent event) {
          if (mb.getHideButton() == mb.getButtonById(Dialog.PredefinedButton.YES.name())) {
            final Customer selectedItem = grid.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
              _crmService.removeCustomer(selectedItem, new AsyncCallback<Void>() {
                @Override
                public void onSuccess(Void result) {
                  grid.getStore().remove(selectedItem);
                }
                @Override
                public void onFailure(Throwable caught) {
                  amb.show();
                }
              });
            }
          } else if (mb.getHideButton() == mb.getButtonById(Dialog.PredefinedButton.NO.name())) {
            // perform NO action
          }
        }
      });
    }

    @Override
    public void onClick(ClickEvent event) {
      final Customer selectedItem = grid.getSelectionModel().getSelectedItem();
      if (selectedItem != null) {
        mb.show();
      }
    }
  }

  private class SubmitButtonHandler implements ClickHandler {
    @Override
    public void onClick(ClickEvent event) {
      final Customer customerFromEditor = driver.flush();
      boolean newCustomer = customerFromEditor.getMemberId() == null;
      if (newCustomer) {
        _crmService.addCustomer(customerFromEditor, new AsyncCallback<Customer>() {
          @Override
          public void onSuccess(Customer result) {
            grid.getStore().add(result);
          }
          @Override
          public void onFailure(Throwable caught) {
            new AlertMessageBox("Alert", "Can't add new customer").show();
          }
        });
      } else {
        _crmService.updateCustomer(customerFromEditor, new AsyncCallback<Void>() {
          @Override
          public void onSuccess(Void result) {
            grid.getStore().update(customerFromEditor);
          }
          @Override
          public void onFailure(Throwable caught) {
            new AlertMessageBox("Alert", "Can't add new customer").show();
          }
        });
      }
      widget.add(infoPanel);
    }
  }

  private class CancelButtonHandler implements ClickHandler {
    @Override
    public void onClick(ClickEvent event) {
      driver.edit(null);
      widget.add(infoPanel);
    }
  }
}
