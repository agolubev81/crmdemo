package demo.crm.web.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import demo.crm.model.client.Customer;
import demo.crm.web.server.api.CrmAdministratorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CrmAdministratorServiceImpl extends RemoteServiceServlet implements CrmAdministratorService {
  private Logger _logger = LoggerFactory.getLogger(getClass());

  @Override
  public Customer addCustomer(Customer customer) throws Exception {
    return null;
  }

  @Override
  public void updateCustomer(Customer customer) throws Exception {

  }

  @Override
  public void removeCustomer(Customer customer) throws Exception {

  }

  @Override
  public Customer[] getCustomers() {
    return new Customer[0];
  }
}
