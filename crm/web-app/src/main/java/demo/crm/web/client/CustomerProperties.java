package demo.crm.web.client;

import java.util.List;

import com.google.gwt.editor.client.Editor.Path;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;
import demo.crm.model.client.Customer;
import demo.crm.model.client.PhoneNumber;

public interface CustomerProperties extends PropertyAccess<Customer> {
  @Path("memberId")
  ModelKeyProvider<Customer> key();

  ValueProvider<Customer, String> firstName();

  ValueProvider<Customer, String> lastName();

  ValueProvider<Customer, List<PhoneNumber>> phoneNumbers();

}