package demo.crm.web.client;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.client.editor.ListStoreEditor;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;
import com.sencha.gxt.widget.core.client.button.ButtonBar;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.FormPanel.LabelAlign;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.grid.editing.GridInlineEditing;
import demo.crm.model.client.Customer;
import demo.crm.model.client.PhoneNumber;

public class CustomerEditor implements IsWidget, Editor<Customer> {

  interface PhoneNumberProperties extends PropertyAccess<PhoneNumber> {
    @Path("number")
    ModelKeyProvider<PhoneNumber> key();
//    @Path("number")
//    IdentityValueProvider<PhoneNumber> id();
    ValueProvider<PhoneNumber, String> number();
    ValueProvider<PhoneNumber, String> description();
  }

  private static final PhoneNumberProperties props = GWT.create(PhoneNumberProperties.class);

  TextField firstName = new TextField();
  TextField lastName = new TextField();

  ListStore<PhoneNumber> phoneNumbersStore = new ListStore<PhoneNumber>(props.key());
  ListStoreEditor<PhoneNumber> phoneNumbers = new ListStoreEditor<PhoneNumber>(phoneNumbersStore);

  private FlowPanel container;

  @Override
  public Widget asWidget() {
    if (container == null) {
      container = new FlowPanel();

      // should be layout based
      int w = 275;
      firstName.setWidth(w);
      firstName.setAllowBlank(false);
      lastName.setWidth(w);

      container.add(new FieldLabel(firstName, "First Name"));
      container.add(new FieldLabel(lastName, "Last Name"));

      List<ColumnConfig<PhoneNumber, ?>> columns = new ArrayList<ColumnConfig<PhoneNumber,?>>();
      ColumnConfig<PhoneNumber, String> number = new ColumnConfig<PhoneNumber, String>(props.number(), 100, "Number");
      columns.add(number);
      ColumnConfig<PhoneNumber, String> description = new ColumnConfig<PhoneNumber, String>(props.description(), 100, "Description");
      columns.add(description);

      final Grid<PhoneNumber> grid = new Grid<PhoneNumber>(phoneNumbersStore, new ColumnModel<PhoneNumber>(columns));
      grid.setBorders(true);
//      grid.setSelectionModel(selection);

      grid.getView().setForceFit(true);
      GridInlineEditing<PhoneNumber> inlineEditor = new GridInlineEditing<PhoneNumber>(grid);
      inlineEditor.addEditor(number, new TextField());
      inlineEditor.addEditor(description, new TextField());

      grid.setWidth(382);
      grid.setHeight(75);

      FieldLabel phonesContainer = new FieldLabel();
      phonesContainer.setText("Phones");
      phonesContainer.setLabelAlign(LabelAlign.TOP);
      phonesContainer.setWidget(grid);
      container.add(phonesContainer);

      TextButton deleteBtn = new TextButton("Delete selected phones", new SelectHandler() {
        @Override
        public void onSelect(SelectEvent event) {
          for (PhoneNumber entityToDelete : grid.getSelectionModel().getSelectedItems()) {
            phoneNumbersStore.remove(entityToDelete);
          }
        }
      });
      TextButton createBtn = new TextButton("Create new phone", new SelectHandler() {
        @Override
        public void onSelect(SelectEvent event) {
          PhoneNumber newRow = new PhoneNumber("", "New phone");
          phoneNumbersStore.add(newRow);
        }
      });
      ButtonBar buttons = new ButtonBar();
      buttons.add(deleteBtn);
      buttons.add(createBtn);
      container.add(buttons);
    }
    return container;
  }

}
