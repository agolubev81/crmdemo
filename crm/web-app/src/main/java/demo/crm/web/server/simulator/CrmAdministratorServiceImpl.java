package demo.crm.web.server.simulator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import demo.crm.model.client.Customer;
import demo.crm.model.client.PhoneNumber;
import demo.crm.web.server.api.CrmAdministratorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CrmAdministratorServiceImpl extends RemoteServiceServlet implements CrmAdministratorService {

  private Logger _logger = LoggerFactory.getLogger(getClass());

  private static Map<String, Customer> _customers = new ConcurrentHashMap<>();
  private static AtomicInteger idCounter = new AtomicInteger(0);

  public CrmAdministratorServiceImpl() {
    Customer customer;

    customer = new Customer().
            setMemberId("id001").
            setFirstName("Chuck").
            setLastName("Norris").
            setPhoneNumbers(new ArrayList(
                    Arrays.asList(new PhoneNumber("+1(202)7654321", "Toll free"))));
    _customers.put(customer.getMemberId(), customer);

    customer = new Customer().
            setMemberId("id007").
            setFirstName("James").
            setLastName("Bond").
            setPhoneNumbers(new ArrayList(Arrays.asList(
                    new PhoneNumber("+44(253)1234567", "Never call never"))));
    _customers.put(customer.getMemberId(), customer);
  }

  @Override
  public Customer addCustomer(Customer customer) throws Exception {
    customer.setMemberId(String.valueOf(idCounter.incrementAndGet()));
    _customers.put(customer.getMemberId(), customer);
    return customer;
  }

  @Override
  public void updateCustomer(Customer customer) throws Exception {
    String memberId = customer.getMemberId();
    if (memberId == null) {
      throw new Exception("Can't update customer without memberId info");
    }
    _customers.put(memberId, customer);
  }

  @Override
  public void removeCustomer(Customer customer) throws Exception {
    String memberId = customer.getMemberId();
    if (memberId == null) {
      throw new Exception("Can't remove customer without memberId info");
    }
    _customers.remove(memberId);
  }

  @Override
  public Customer[] getCustomers() {
    return _customers.values().toArray(new Customer[0]);
  }
}
