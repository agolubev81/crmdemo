@echo off
rem ---------------------------------------------------------------------------
rem Start Script for Tomcat Web Server
rem
rem Environment Variable Prequisites
rem
rem   CATALINA_HOME   Must be present and point at your Catalina(Tomcat) install directory.
rem
rem   CATALINA_BASE   (Optional) Base directory for the templates web application installation.
rem                   If not present, resolves to current directory (default).
rem
rem   JAVA_HOME       Must point at your Java Development Kit installation.
rem                   Required to run the with the "debug" argument.
rem
rem   JRE_HOME        Must point at your Java Runtime installation.
rem                   Defaults to JAVA_HOME if empty.
rem
rem   Set CATALINA_OPTS to specify additional options like below:
rem                   CATALINA_OPTS=-Djava.security.debug=policy,ssl
rem
rem ---------------------------------------------------------------------------

rem set JAVA_HOME=C:\Java\jdk1.6
rem set CATALINA_HOME=C:\Java\tomcat-7.0

echo CATALINA_HOME=%CATALINA_HOME%

if not exist "%CATALINA_HOME%\bin\catalina.bat" goto noRuntime

set CURRENT_DIR=%cd%
if not "%CATALINA_BASE%" == "" goto okBase
set CATALINA_BASE=%CURRENT_DIR%
:okBase

echo CATALINA_BASE=%CATALINA_BASE%
set CATALINA_TMPDIR=%CATALINA_BASE%\temp

SET JAVA_DEBUG_OPTS=-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=1044
# Default timezone settings is required for accessing data of 'date' type from Postgres without 
# adjustments which could be made by jdbc driver if local timezone is different from UTC/GMT
SET JAVA_TIMEZONE=-Duser.timezone=GMT
SET JAVA_SERVER_OPTS=-server -Xms1024M -Xmx1024M %JAVA_TIMEZONE%
SET JAVA_OPTS=%JAVA_SERVER_OPTS%
rem set JAVA_OPTS=%JAVA_SERVER_OPTS% %JAVA_DEBUG_OPTS%
rem set JAVA_OPTS=%JAVA_SERVER_OPTS%
rem set JAVA_OPTS=-Dderby.stream.error.file=%CATALINA_BASE%\logs\derby.log
rem set CATALINA_OPTS=-Djava.security.debug=policy,ssl
echo JAVA_OPTS=%JAVA_OPTS%

@echo on
"%CATALINA_HOME%\bin\catalina.bat" run
goto end

:noRuntime
echo The CATALINA_HOME environment variable is not defined correctly
echo This environment variable is needed to run this program
goto end

:end
