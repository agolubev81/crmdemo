package demo.crm.web.server.api;

import demo.crm.model.client.Customer;

public interface CrmService {

  // TODO: add methods for customers search and partial fetch

  /**
   * Fetches all customers registered in system, can be quite big piece of data
   *
   * @return
   */
  Customer[] getCustomers();
}
