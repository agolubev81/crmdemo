package demo.crm.web.server.api;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import demo.crm.model.client.Customer;

@RemoteServiceRelativePath("admin")
public interface CrmAdministratorService extends CrmService, RemoteService {

  /**
   * Fetches all customers registered in system, can be quite big piece of data
   *
   * @return
   */
  @Override
  Customer[] getCustomers();

  /**
   * Adds a new customer
   *
   * @param customer some available information about new customer, if it's null then new customer will have only memberId after creation
   * @return new customer
   * @throws Exception if customer can't be added for some reason
   */
  Customer addCustomer(Customer customer) throws Exception;

  /**
   * Updates information for specified customer
   *
   * @param customer with state to be updated in system
   * @throws Exception if specified customer can't be found or information is invalid
   */
  void updateCustomer(Customer customer) throws Exception;

  /**
   * Removes a specified customer
   *
   * @param customer
   * @throws Exception if customer can't be removed
   */
  void removeCustomer(Customer customer) throws Exception;
}
