package demo.crm.web.server.api;

import com.google.gwt.user.client.rpc.AsyncCallback;
import demo.crm.model.client.Customer;

public interface CrmAdministratorServiceAsync {

  /**
   * Fetches all customers, can be quite big piece of data
   *
   * @param async
   */
  void getCustomers(AsyncCallback<Customer[]> async);

  /**
   * Adds a new customer
   *
   * @param customer some available information about new customer, if it's null then new customer will have only memberId after creation
   * @return new customer
   * @throws Exception if customer can't be added for some reason
   */
  void addCustomer(Customer customer, AsyncCallback<Customer> async);

  /**
   * Updates information for specified customer
   *
   * @param customer with state to be updated in system
   * @throws Exception if specified customer can't be found or information is invalid
   */
  void updateCustomer(Customer customer, AsyncCallback<Void> async);

  /**
   * Removes a specified customer
   *
   * @param customer
   * @throws Exception if customer can't be removed
   */
  void removeCustomer(Customer customer, AsyncCallback<Void> async);

}
