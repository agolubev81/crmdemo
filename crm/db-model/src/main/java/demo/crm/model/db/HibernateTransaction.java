package demo.crm.model.db;

import org.hibernate.Session;

public interface HibernateTransaction extends Transaction {
  Session getSession();
}
