package demo.crm.model.db;

public class PhoneNumberRec implements java.io.Serializable, Cloneable {
  private long id;
  private String number;
  private String description;
  private CustomerRec customerRec;

  public PhoneNumberRec() {
  }

  public PhoneNumberRec(String number, String description) {
    this.number = number;
    this.description = description;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setCustomerRec(CustomerRec customerRec) {
    this.customerRec = customerRec;
  }

  public CustomerRec getCustomerRec() {
    return customerRec;
  }

  //----------------------------------------------------------------------------

  @Override
  protected Object clone() {
    PhoneNumberRec clone;
    try {
      clone = (PhoneNumberRec)super.clone();
    } catch(CloneNotSupportedException e) {
      throw new RuntimeException(e);
    }
    return clone;
  }

  @Override
  public boolean equals(Object o) {
    if(this == o) return true;
    if(!(o instanceof PhoneNumberRec)) return false;

    PhoneNumberRec other = (PhoneNumberRec)o;
    long otherId = other.getId();

    if(id != otherId) return false;

    return true;
  }

  @Override
  public int hashCode() {
    return (int)(id ^ (id >>> 32));
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("{");
    sb.append("number=").append(number);
    sb.append(", description=").append(description);
    return sb.append('}').toString();
  }
}
