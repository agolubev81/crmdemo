package demo.crm.model.db;

import org.hibernate.Session;

public class PhoneNumberRecDAOHibernateImpl extends AbstractHibernateDAO<PhoneNumberRec,String> implements PhoneNumberRecDAO {

  public PhoneNumberRecDAOHibernateImpl(Session session) {
    super(session);
  }

  public PhoneNumberRecDAOHibernateImpl() {
  }
}
