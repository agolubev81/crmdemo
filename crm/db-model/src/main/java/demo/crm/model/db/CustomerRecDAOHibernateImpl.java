package demo.crm.model.db;

import org.hibernate.Session;

public class CustomerRecDAOHibernateImpl extends AbstractHibernateDAO<CustomerRec,String> implements CustomerRecDAO {

  public CustomerRecDAOHibernateImpl(Session session) {
    super(session);
  }

  public CustomerRecDAOHibernateImpl() {
  }
}
