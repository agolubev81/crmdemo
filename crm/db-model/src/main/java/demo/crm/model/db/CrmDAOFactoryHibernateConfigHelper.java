package demo.crm.model.db;

import java.util.Properties;

public class CrmDAOFactoryHibernateConfigHelper {
  
  public static synchronized CrmDAOFactoryHibernateConfig getLocalhostPostgresConfig() {
//    File hibernateConfig = null;
//    try {
//      hibernateConfig = new File(TemplatesDAOFactoryHibernateConfig.class.getClassLoader().getResource(configResourcePath).toURI());
//    } catch (URISyntaxException e) {
//      throw new RuntimeException(e);
//    }
    Properties properties = new Properties();
//    properties.setProperty("hibernate.connection.driverClass", "org.postgresql.Driver");
//    properties.setProperty("hibernate.connection.url", "jdbc:postgresql://localhost:5432/postgres");
//    properties.setProperty("hibernate.connection.username", "postgres");
//    properties.setProperty("hibernate.connection.password", "postgrespass");
//    properties.setProperty("hibernate.dialect.dialect", "org.hibernate.dialect.PostgreSQL82Dialect");
    return getLocalhostPostgresConfig(properties);
  }

  public static synchronized CrmDAOFactoryHibernateConfig getLocalhostPostgresConfig(Properties properties) {
    String configResourcePath = "META-INF/hibernate.cfg.xml";
    CrmDAOFactoryHibernateConfig config = new CrmDAOFactoryHibernateConfig();
    config.configure(configResourcePath, properties);
    return config;
  }
}

