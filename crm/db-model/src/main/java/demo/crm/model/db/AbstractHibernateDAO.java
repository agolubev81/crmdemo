package demo.crm.model.db;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import javax.validation.constraints.NotNull;

import org.hibernate.Criteria;
import org.hibernate.LockOptions;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;

public abstract class AbstractHibernateDAO<T, ID extends Serializable> implements GenericDAO<T, ID> {
  private Class<T> persistentClass;
  private Session session;

  @SuppressWarnings("unchecked")
  public AbstractHibernateDAO(Session session) {
    this.persistentClass = (Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    this.session = session;
  }
  @SuppressWarnings("unchecked")
  public AbstractHibernateDAO() {
    this.persistentClass = (Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    this.session = null;
  }

  @SuppressWarnings("unchecked")
  public void setSession(Session s) {
    this.session = s;
  }

  protected Session getSession() {
    if(session == null)
      throw new IllegalStateException("Session has not been set on DAO before usage");
    return session;
  }

  public Class<T> getPersistentClass() {
    return persistentClass;
  }

  @SuppressWarnings("unchecked")
  @NotNull
  public T loadById(ID id, boolean lock) {
    T entity;
    if(lock)
      entity = (T)getSession().load(getPersistentClass(), id, LockOptions.UPGRADE);
    else
      entity = (T)getSession().load(getPersistentClass(), id);

    return entity;
  }

  @SuppressWarnings("unchecked")
  @NotNull
  public T loadById(ID id) {
    return (T)getSession().load(getPersistentClass(), id);
  }

  @SuppressWarnings("unchecked")
  public T findById(ID id, boolean lock) {
    T entity;
    if(lock)
      entity = (T)getSession().get(getPersistentClass(), id, LockOptions.UPGRADE); // alternatively use "load"
    else
      entity = (T)getSession().get(getPersistentClass(), id); // alternatively use "load"

    return entity;
  }

  @SuppressWarnings("unchecked")
  public T findById(ID id) {
    return (T)getSession().get(getPersistentClass(), id); // alternatively use "load"
  }

  @SuppressWarnings("unchecked")
  public List<T> findAll() {
    return findByCriteria();
  }

  @SuppressWarnings("unchecked")
  public T save(T entity) {
    getSession().save(entity);
    return entity;
  }

  @SuppressWarnings("unchecked")
  public T update(T entity) {
    getSession().update(entity);
    return entity;
  }

  @SuppressWarnings("unchecked")
  public T saveOrUpdate(T entity) {
    getSession().saveOrUpdate(entity);
    return entity;
  }

  @SuppressWarnings("unchecked")
  public T persist(T entity) {
    getSession().persist(entity);
    return entity;
  }

  public void delete(T entity) {
    getSession().delete(entity);
  }

  public void flush() {
    getSession().flush();
  }

  public void clear() {
    getSession().clear();
  }

  /**
   * Use this inside subclasses as a convenience method.
   */
  @SuppressWarnings("unchecked")
  protected List<T> findByCriteria(Criterion... criterion) {
    Criteria criteria = getSession().createCriteria(getPersistentClass());
    for(Criterion c: criterion) {
      criteria.add(c);
    }
    return criteria.list();
  }
}
