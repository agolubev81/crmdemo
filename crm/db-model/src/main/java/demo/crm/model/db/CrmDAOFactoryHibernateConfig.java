package demo.crm.model.db;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.cfg.Configuration;
import org.hibernate.context.internal.ManagedSessionContext;

import java.util.Properties;

public class CrmDAOFactoryHibernateConfig {
  private SessionFactory sessionFactory;
  private Configuration config;

  public CrmDAOFactoryHibernateConfig() {

  }

  public SessionFactory configure(String configFile, Properties properties) {
    config = new Configuration();
    config.configure(configFile); // configures settings from hibernate.cfg.xml
    config.addProperties(properties);
    sessionFactory = config.buildSessionFactory();
    return sessionFactory;
  }

  public Configuration getConfig() {
    return config;
  }

  public SessionFactory getSessionFactory() {
    return sessionFactory;
  }

  public Session openSession() {
    Session session = sessionFactory.openSession();
    ManagedSessionContext.bind(session);
    return session;
  }

  public StatelessSession openStatelessSession() {
    return sessionFactory.openStatelessSession();
  }

  public void closeSession(Object session) {
    if(session instanceof Session) {
      ManagedSessionContext.unbind(sessionFactory);
      ((Session)session).close();
    } else if(session instanceof StatelessSession) {
      ((StatelessSession)session).close();
    } else {
      if(session == null)
        throw new NullPointerException("session");
      else
        throw new IllegalArgumentException(session.getClass()+" is not a session");
    }
  }

  public void teardown() {
    sessionFactory.close();
  }
}
