package demo.crm.model.db;

import java.io.Serializable;
import java.util.List;

public interface GenericDAO<T, ID extends Serializable> {
  T loadById(ID id);
  T loadById(ID id, boolean lock);
  T findById(ID id);
  T findById(ID id, boolean lock);
  List<T> findAll();

  T save(T entity);
  T update(T entity);
  T saveOrUpdate(T entity);
  T persist(T entity);
  void delete(T entity);

  void flush();
  void clear();
}
