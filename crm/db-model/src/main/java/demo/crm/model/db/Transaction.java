package demo.crm.model.db;

public interface Transaction {
  void flush();
  void clear();

  void commit();
  void rollback();
  boolean isActive();
}
