package demo.crm.model.db;

import java.io.Serializable;
import java.util.Collection;

public class CustomerRec implements Serializable, Cloneable {

  private String memberId;
  private String firstName;
  private String lastName;
  private Collection<PhoneNumberRec> phoneNumberRecs;

  public CustomerRec() {
  }

  public CustomerRec(String memberId, String firstName, String lastName, Collection<PhoneNumberRec> phoneNumbers) {
    this.memberId = memberId;
    this.firstName = firstName;
    this.lastName = lastName;
    phoneNumberRecs = phoneNumbers;
  }

  public String getMemberId() {
    return memberId;
  }

  public void setMemberId(String memberId) {
    this.memberId = memberId;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Collection<PhoneNumberRec> getPhoneNumberRecs() {
    return phoneNumberRecs;
  }

  public void setPhoneNumberRecs(Collection<PhoneNumberRec> phoneNumberRecs) {
    for (PhoneNumberRec tcr : phoneNumberRecs) {
      tcr.setCustomerRec(this);
    }
    this.phoneNumberRecs = phoneNumberRecs;
  }

  //----------------------------------------------------------------------------

  @Override
  protected Object clone() {
    CustomerRec clone;
    try {
      clone = (CustomerRec)super.clone();
    } catch(CloneNotSupportedException e) {
      throw new RuntimeException(e);
    }
    return clone;
  }

  @Override
  public boolean equals(Object o) {
    if(this == o) return true;
    if(!(o instanceof CustomerRec)) return false;

    CustomerRec other = (CustomerRec)o;
    String otherId = other.getMemberId();

    if(memberId != null ? !memberId.equals(otherId) : otherId != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    return memberId != null ? memberId.hashCode() : 0;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("{");
    sb.append("firstName=").append(firstName);
    sb.append(", lastName=").append(lastName);
    if (phoneNumberRecs != null) {
      sb.append(", phones=").append(phoneNumberRecs);
    }
    sb.append('}');
    return sb.toString();
  }
}
