package demo.crm.model.db;

import org.hibernate.Session;

public class CrmDAOFactoryHibernateImpl extends CrmDAOFactory {

  private final Session session;

  //public TemplatesDAOFactoryHibernateImpl(SessionFactory sessionFactory) {
  //  this.session = sessionFactory.getCurrentSession();
  //}

  public CrmDAOFactoryHibernateImpl(Session session) {
    this.session = session;
  }

  private AbstractHibernateDAO instantiateDAO(Class daoClass) {
    try {
      AbstractHibernateDAO dao = (AbstractHibernateDAO)daoClass.newInstance();
      dao.setSession(getCurrentSession());
      return dao;
    } catch(Exception ex) {
      throw new RuntimeException("Can not instantiate DAO: " + daoClass, ex);
    }
  }

  // You could override this if you don't want HibernateUtil for lookup
  public Session getCurrentSession() {
    return session;
  }

  @Override
  public Transaction startTransaction() {
    return new HibernateTransactionImpl(getCurrentSession());
  }

  public void flush() {
    getCurrentSession().flush();
  }

  public void clear() {
    getCurrentSession().clear();
  }

  public CustomerRecDAO getCustomerRecDAO() {
    return (CustomerRecDAO)instantiateDAO(CustomerRecDAOHibernateImpl.class);
  }

  public PhoneNumberRecDAO getPhoneNumberRecDAO() {
    return (PhoneNumberRecDAO) instantiateDAO(PhoneNumberRecDAOHibernateImpl.class);
  }

}