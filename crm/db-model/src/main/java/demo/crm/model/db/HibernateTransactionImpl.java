package demo.crm.model.db;

public class HibernateTransactionImpl implements HibernateTransaction {
  private final org.hibernate.Transaction transaction;
  private final org.hibernate.Session session;

  HibernateTransactionImpl(org.hibernate.Session session) {
    this.session = session;
    this.transaction = session.beginTransaction();
  }

  @Override
  public org.hibernate.Session getSession() {
    return session;
  }

  @Override
  public void flush() {
    session.flush();
  }

  @Override
  public void clear() {
    session.clear();
  }

  @Override
  public void commit() {
    transaction.commit();
  }

  @Override
  public void rollback() {
    transaction.rollback();
  }

  @Override
  public boolean isActive() {
    return transaction.isActive();
  }
}
