package demo.crm.model.db;

import org.hibernate.Session;
import org.hibernate.Transaction;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

public abstract class CrmDAOFactory {

  public static String HIBERNATE = "hibernate";

  private static final Map<String,DAOProvider> _providers;
  static {
    _providers = new HashMap<String, DAOProvider>(1);
    _providers.put(HIBERNATE, new DAOProvider(CrmDAOFactory.class, new Class<?>[]{Session.class}));
  }

  private static final class DAOProvider {
    Class providerClass;
    Class<?>[] providerArguments;
    DAOProvider(Class providerClass, Class<?>[] providerArguments) {
      this.providerClass = providerClass;
      this.providerArguments = providerArguments;
    }
  }

  /**
   * Factory method for instantiation of concrete factories.
   */
  public static CrmDAOFactory instance(String provider, Object... args) {
    DAOProvider rec = _providers.get(provider);
    if(rec == null)
      throw new RuntimeException("Couldn't find CrmDAOFactory: " + provider);
    try {
      Constructor<? extends CrmDAOFactory> c = rec.providerClass.getConstructor(rec.providerArguments);
      return c.newInstance(args);
    } catch(Exception ex) {
      throw new RuntimeException("Couldn't create CrmDAOFactory: " + rec.providerClass.getCanonicalName(), ex);
    }
  }

  public abstract demo.crm.model.db.Transaction startTransaction();
  public abstract void flush();//TODO: remove this
  public abstract void clear();//TODO: remove this

  public abstract CustomerRecDAO getCustomerRecDAO();

  public abstract PhoneNumberRecDAO getPhoneNumberRecDAO();

}
