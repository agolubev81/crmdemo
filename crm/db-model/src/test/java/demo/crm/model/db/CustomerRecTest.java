package demo.crm.model.db;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

public class CustomerRecTest extends TestCase {

  public void testDAO() throws Exception {
    CrmDAOFactoryHibernateConfig localhostPostgresConfig = CrmDAOFactoryHibernateConfigHelper.getLocalhostPostgresConfig();
    CrmDAOFactoryHibernateImpl crmDAOFactoryHibernate = new CrmDAOFactoryHibernateImpl(localhostPostgresConfig.openSession());
    CustomerRecDAO customerRecDAO = crmDAOFactoryHibernate.getCustomerRecDAO();

    CustomerRec customerRec = new CustomerRec();
    customerRec.setMemberId("id000001");
    customerRec.setFirstName("My First Name");
    customerRec.setLastName("My Last Name");

    List<PhoneNumberRec> phoneNumberRecs = new ArrayList<>();
    phoneNumberRecs.add(new PhoneNumberRec("12345", "description1"));
    phoneNumberRecs.add(new PhoneNumberRec("54321", "description2"));

    customerRec.setPhoneNumberRecs(phoneNumberRecs);

    customerRecDAO.persist(customerRec);
    customerRecDAO.flush();

    for (CustomerRec customer : customerRecDAO.findAll()) {
      System.out.println(customer);
    }

    crmDAOFactoryHibernate.getCurrentSession().close();
  }
}
